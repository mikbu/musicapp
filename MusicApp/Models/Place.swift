//
//  Place.swift
//  MusicApp
//
//  Created by Mikołaj Bujok on 23.04.2018.
//  Copyright © 2018 Mikołaj. All rights reserved.
//

import Foundation

struct Place: Codable {

    var id: String
    var type: PlaceTypeEnum?
    var name: String?
    var coordinates: Coords?
    var lifeSpan: LifeSpan
    var score: String?
    var address: String?

    enum CodingKeys: String, CodingKey {
        case lifeSpan = "life-span",
        id,
        type,
        name,
        coordinates,
        score,
        address
    }
}

enum PlaceTypeEnum: String, Codable {
    case studio = "Studio",
    venue = "Venue",
    stadium = "Stadium",
    indoorArena = "Indoor arena",
    religiousBuilding = "ReligiousBuilding",
    other = "Other",
    edu = "Educational institution"
}
