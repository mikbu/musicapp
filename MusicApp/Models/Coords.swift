//
//  Coords.swift
//  MusicApp
//
//  Created by mbujok on 23.04.2018.
//  Copyright © 2018 Mikołaj. All rights reserved.
//

import Foundation

struct Coords: Codable {
    var latitude: Double
    var longitude: Double

    init(latitude: Double, longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let latitudeString = try container.decode(String.self, forKey: .latitude)
        guard let latitude = Double(latitudeString) else {
            throw NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Latitude is corrupted"])
        }

        let longitudeString = try container.decode(String.self, forKey: .longitude)
        guard let longitude = Double(longitudeString) else {
            throw NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Longitude is corrupted"])
        }

        self.init(latitude: latitude, longitude: longitude)
    }

    enum CodingKeys: String, CodingKey {
        case latitude, longitude
    }
}
