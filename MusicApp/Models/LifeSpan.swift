//
//  LifeSpan.swift
//  MusicApp
//
//  Created by mbujok on 23.04.2018.
//  Copyright © 2018 Mikołaj. All rights reserved.
//

import Foundation

struct LifeSpan: Codable {
    var lifeTime: Int

    init(lifeTime: Int) {
        self.lifeTime = lifeTime
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let dateString = try container.decode(String.self, forKey: .lifeTime)

        guard let beginYear = Int(dateString.prefix(4)) else {
            self.init(lifeTime: 0)
            return
        }
        self.init(lifeTime: beginYear - 1990)
    }

    enum CodingKeys: String, CodingKey {
        case lifeTime = "begin"
    }
}
