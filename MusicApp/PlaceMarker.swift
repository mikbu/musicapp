//
//  PlaceMarker.swift
//  MusicApp
//
//  Created by Mikołaj Bujok on 23.04.2018.
//  Copyright © 2018 Mikołaj. All rights reserved.
//

import Foundation
import MapKit

protocol AnnotationChangeDelegate: class {
    func remove(marker: PlaceMarker)
}

class PlaceMarker: NSObject, MKAnnotation {

    let id: String
    let title: String?
    let locationType: PlaceTypeEnum
    let lifeTime: Int
    let coordinate: CLLocationCoordinate2D

    private var timer: Timer?
    weak var delegate: AnnotationChangeDelegate?

    init(id: String, title: String, lifeSpan: Int, locationType: PlaceTypeEnum, coordinate: CLLocationCoordinate2D) {
        self.id = id
        self.title = title
        self.lifeTime = lifeSpan
        self.locationType = locationType
        self.coordinate = coordinate

        super.init()
    }

    var subtitle: String? {
        return locationType.rawValue
    }

    var imageName: String? {
        switch locationType {
        case .venue:
            return "venue"
        case .stadium:
            return "arena"
        case .religiousBuilding:
            return "chapel"
        default:
            return "music"
        }
    }

    /**
     Deletes itself from map view
     */
    @objc func selfDestruct() {
        delegate!.remove(marker: self)
    }

    /**
     Starts life of a marker based on it'a lifeTime
     - parameter delegate: delegate for view controller that manages the map where marker is being displayed
     */
    func startLife(_ delegate: AnnotationChangeDelegate) {

        self.delegate = delegate
        self.timer = Timer.scheduledTimer(timeInterval: TimeInterval(self.lifeTime), target: self, selector: #selector(self.selfDestruct), userInfo: nil, repeats: false)
    }
}
